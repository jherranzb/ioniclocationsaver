angular.module('starter')

.controller('AccountCtrl', function($scope, $cordovaFileTransfer, $ionicPlatform, $ionicLoading, $compile, $http, $cordovaSQLite, Usuarios, Positions, Images) {

  $scope.idUsuario = 1;
  $scope.mensaje = '';
  $scope.datos = {
    idUsuario: Usuarios.getActiveUserData('id')
  }


  $ionicPlatform.ready(function() {
    var usuarios = [];
    var posiciones = [];
    var imagenes = [];

    usuarios = Usuarios.getAllUsersData();
    console.log("Hay " + usuarios.length + " usuarios");
    $scope.usuarios = usuarios;

    Positions.getPositions().then(function(res){
      posiciones = res;

      var numPosiciones = posiciones.length;

      if( numPosiciones > 0){
        $scope.mensaje = "Hay " + numPosiciones + " posiciones nuevas para sincronizar";
      } else {
        $scope.mensaje = "No hay posiciones nuevas para sincronizar";
      }
    });
    
    imagenes = Images.getImages(); 

    /*if( numPosiciones > 0){
      $scope.mensaje = "Hay " + numPosiciones + " posiciones nuevas para sincronizar";
    } else {
      $scope.mensaje = "No hay posiciones nuevas para sincronizar";
    }*/

  
  }, function(err) {
      // error
  });   

  $scope.changeActiveUser = function(){
    Usuarios.setActiveUser($scope.datos.idUsuario);
  }


  $scope.sync = function() {

    $http({
      method: 'POST',
      //headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      url: "http://jherranz.sytes.net/locationSaver/insertPositions.php",
      data: { 'positions' : posiciones,
              'images' : imagenes } 
    })
    .success(function (data, status, headers, config) { 
      console.log('Correcto');
      console.log(data);
    })
    .error(function (data, status, headers, config) { 
      console.log('Error');
      console.log('Error status: ' + status);
      console.log('Error data: ' + data);
    });

    var nombreImagen = imagenes[0]['nombreImagen'];

  }, false);
  }  

});
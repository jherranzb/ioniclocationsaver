angular.module('starter')

.controller('ChatDetailCtrl', function($scope, $stateParams, $cordovaSQLite, $ionicModal, $cordovaSocialSharing, Positions) {
  $scope.images = [];
  $scope.allImages = [];
  /*$scope.datos = {
    descripcion: '';
  }*/
  $scope.datos = {
    descripcion: ''
  }
  var idPosicion = $stateParams.chatId;
  var query = "SELECT * FROM imagenes where idPosicion = " + idPosicion;
  console.log(query);
  $scope.nombre= query;
  $cordovaSQLite.execute(db, query).then(function(res) {
      var contador = 0;
      if(res.rows.length > 0) {
        console.log('He encontrado ' + res.rows.length + ' imagenes');
        for(contador = 0; contador < res.rows.length; contador++){
          $scope.images.push(res.rows.item(contador).nombreImagen);
        }
      } else {
        console.log("No results found");
      }
  }, function (err) {
      console.error(err);
  });

  Positions.getDataPositions(idPosicion).then(function(res){
    $scope.datos.descripcion = res[0]['descripcion'];
    console.log($scope.datos.descripcion);
  });

  $scope.share = function(imagen){
    /*$cordovaSocialSharing
    .shareViaWhatsApp('message', imagen, 'www.google.es')
    .then(function(result) {
      console.log('Enviado');
    }, function(err) {
      console.log('Error');
      // An error occurred. Show a message to the user
    });*/
    console.log('Entramos a compartir por whatsapp ' + imagen)
  }

  $scope.updatePosition = function(){
    Positions.updatePosition(idPosicion, $scope.datos.descripcion).then(function(res){
      console.log('Aleeeeeeeee');
    });
    console.log('Actalizando');
  }



  $scope.urlForImage = function(imageName) {
    var name = imageName.substr(imageName.lastIndexOf('/') + 1);
    var trueOrigin = cordova.file.dataDirectory + name;
    return trueOrigin;
  }
  //$scope.chat = Chats.get($stateParams.chatId);

  $scope.showImages = function(index) {
    console.log('Me llega el index ' + index);
    $scope.activeSlide = index;
    $scope.showModal('templates/image-popover.html');
  }
 
  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
 
  // Close the modal
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };
});
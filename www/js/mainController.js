angular.module('starter')

.controller('DashCtrl', function($scope, System, $ionicModal, $cordovaSQLite, Camera, $cordovaImagePicker, $ionicPlatform, $cordovaGeolocation, $ionicPopup, $http, Usuarios) {
  $scope.nombre = 'pepito';
  $scope.latitud = '';
  $scope.longitud = '';
  $scope.mensaje = '';
  $scope.datos = {
    descripcion: ''
  };

  //GEOLOCATION
  console.log('Empezamos!!!!!!!!');
  var posOptions = {timeout: 10000, enableHighAccuracy: false};


  console.log('Primera comprobacion!!!!!!!!!!');
  if (System.checkInternet()) {
    var myLatlng = new google.maps.LatLng(40.416875, -3.703308);
   
    var mapOptions = {
        center: myLatlng,
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  }

  $ionicPlatform.ready(function() {
    console.log('$ionicPlatform ready');
    var idUsuario = Usuarios.getActiveUserData('id');
    console.log('El id del usuario activo es ' + idUsuario);
    $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function (position) {
        $scope.latitud  = position.coords.latitude;
        $scope.longitud = position.coords.longitude;
          if (System.checkInternet()) {
            $http.get("http://jherranz.sytes.net/coordenadas.php?latitud=" + $scope.latitud + "&longitud=" + $scope.longitud).
            success(function(response){ //make a get request to mock json file.
              console.log(response);
              $scope.lugar = response;
            });

            map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            var myLocation = new google.maps.Marker({
                position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                map: map,
                animation: google.maps.Animation.DROP,
                title: $scope.lugar
            });
        } else {
            $scope.lugar = '';
            $scope.mensaje = '¡Posicion obtenida!';
        }

        
      }, function(err) {
        // error
    });
  });


  var watchOptions = {
    timeout : 3000,
    enableHighAccuracy: false // may cause errors if true
  };

  var watch = $cordovaGeolocation.watchPosition(watchOptions);
  watch.then(null,
    function(err) {
      // error
    },
    function(position) {
      $scope.latitud  = position.coords.latitude
      $scope.longitud = position.coords.longitude
  });

  watch.clearWatch();

  $scope.images = [];

  $scope.getPhoto = function() {
    Camera.getPicture({
      allowEdit : false,
      saveToPhotoAlbum: true
    }).then(function(imageURI) {
      $scope.imageUrl = imageURI;
      //$scope.imageUrl = "data:image/jpeg;base64," + imageURI;
      console.log("EL NOMBRE DE LA IMAGEN EEEEEEEEEEES " + imageURI);

      //4
      onImageSuccess(imageURI);
 
    function onImageSuccess(fileURI) {
      createFileEntry(fileURI);
    }
 
    function createFileEntry(fileURI) {
      console.log('Entro a createFileEntry');
      window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
      console.log('Salgo de createFileEntry');
    }
 
    // 5
    function copyFile(fileEntry) {
      console.log('Entro a copyFile');
      var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
      var newName = makeid() + name;
 
      window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(fileSystem2) {
        fileEntry.copyTo(
          fileSystem2,
          newName,
          onCopySuccess,
          fail
        );
      },
      fail);
      console.log('Salgo de copyFile');
    }
    
    // 6
    function onCopySuccess(entry) {
      $scope.$apply(function () {
        $scope.images.push(entry.nativeURL);
        console.log('Acabo de insertar ' + $scope.images[$scope.images .length - 1]);
      });
    }
 
    function fail(error) {
      console.log("fail: " + error.code);
    }
 
    function makeid() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
      for (var i=0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }


    }, function(err) {
      console.err(err);
    });
  }

  $scope.urlForImage = function(imageName) {
    console.log("nombre de la imagen " + imageName);
    var name = imageName.substr(imageName.lastIndexOf('/') + 1);
    console.log("Pequeño nombre de la imagen " + name);
    var trueOrigin = cordova.file.dataDirectory + name;
    return trueOrigin;
  }

  $scope.collection = {
        selectedImage : ''
  };

  $scope.insert = function() {
    var posicionId = -1;
    /*var usuarios = UserStore.getUserStore();*/
    var idUsuario = Usuarios.getActiveUserData('id');
    var query = "INSERT INTO posiciones (latitud, longitud, fecha, lugar, descripcion, activo, idUsuario) VALUES ('" + 
      $scope.latitud + 
      "','" + $scope.longitud + 
      "',strftime('%d-%m-%Y %H:%M', datetime('now')),'" + 
      $scope.lugar + 
      "','" + $scope.datos.descripcion + 
      "', 1, " + idUsuario +")";
    console.log(query);
    $scope.nombre= query;
    $cordovaSQLite.execute(db, query).then(function(res) {
        console.log("INSERT ID -> " + res.insertId);
        posicionId = res.insertId;
        var contadorImagenes = 0;
        console.log('Hay ' + $scope.images.length + ' imagenes para guardar!!!');
        for(contadorImagenes = 0; contadorImagenes < $scope.images.length; contadorImagenes++){
          console.log('guardo ' + (contadorImagenes + 1));
          var query = "INSERT INTO imagenes (idPosicion, nombreImagen) VALUES ('" + posicionId + "','" + $scope.images[contadorImagenes] + "')";
          console.log(query);
          $scope.nombre= query;
          $cordovaSQLite.execute(db, query).then(function(res) {
              console.log("INSERT ID IMAGEN-> " + res.insertId);
          }, function (err) {
              console.error(err);
          });
        }
      $scope.images.length = 0;
      $scope.datos.descripcion = "";
    }, function (err) {
        console.error(err);
    });
  }
 
  $scope.selectPosiciones = function() {
    var query = "SELECT * FROM posiciones where activo = 1";
    $scope.nombre= query;
    $cordovaSQLite.execute(db, query).then(function(res) {
        var contador = 0;
        if(res.rows.length > 0) {
          for(contador = 0; contador < res.rows.length; contador++){
            console.log("SELECTED -> " + contador + " " + res.rows.item(contador).firstname + " " + res.rows.item(contador).lastname);
          }
        } else {
          console.log("No results found");
        }
    }, function (err) {
        console.error(err);
    });
  }
 
  $scope.selectImagenes = function(idPosicion) {
    var query = "SELECT * FROM imagenes where idPosicion = " + idPosicion;
    $scope.nombre= query;
    $cordovaSQLite.execute(db, query).then(function(res) {
        var contador = 0;
        if(res.rows.length > 0) {
          for(contador = 0; contador < res.rows.length; contador++){
            console.log("SELECTED -> " + contador + " " + res.rows.item(contador).firstname + " " + res.rows.item(contador).lastname);
          }
        } else {
          console.log("No results found");
        }
    }, function (err) {
        console.error(err);
    });
  }

  $scope.showImages = function(index) {
    $scope.activeSlide = index;
    $scope.showModal('templates/image-popover.html');
  }
 
  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
 
  // Close the modal
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };
 
  $ionicPlatform.ready(function() {
    $scope.getImageSaveContact = function() {       
      // Image picker will load images according to these settings
      var options = {
        maximumImagesCount: 1, // Max number of selected images, I'm using only one for this example
        width: 800,
        height: 800,
        quality: 80            // Higher is better
      };

      $cordovaImagePicker.getPictures(options).then(function (results) {
        // Loop through acquired images
        for (var i = 0; i < results.length; i++) {
          $scope.collection.selectedImage = results[i];   // We loading only one image so we can use it like this
          window.plugins.Base64.encodeFile($scope.collection.selectedImage, function(base64){  // Encode URI to Base64 needed for contacts plugin
            $scope.collection.selectedImage = base64;
          });
        }
      }, function(error) {
        console.log('Error: ' + JSON.stringify(error));    // In case of error
      });
    };
  }); 
});







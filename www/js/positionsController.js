angular.module('starter')

.controller('ChatsCtrl', function($scope, $cordovaSQLite, Positions, System) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.positions = [];
  var markers = [];

  if (System.checkInternet()) {
    var myLatlng = new google.maps.LatLng(40.416875, -3.703308);
   
    var mapOptions = {
        center: myLatlng,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    console.log('Voy a crear el map');

    var mapa = new google.maps.Map(document.getElementById("mapa"), mapOptions);
    console.log('Mapa creado');
  }

  /*var usuarios = UserStore.getUserStore();
  var idUsuario = usuarios.getActiveUserData('id');*/
  //var query = "SELECT * FROM posiciones where activo = 1 and idUsuario = " + idUsuario;

  Positions.getPositions().then(function(res){
    pintarPosiciones(res);
  });

  /*Positions.getRemotePositions().then(function(){
    console.log('Ole...!!!');
  });*/

  function pintarPosiciones(posiciones){
    var colores = ["80FF00", "FF0040", "FE2EF7", "DF0101", "0101DF", "64FE2E"];
    var contador = 0;
    var numPosiciones = posiciones.length;
    console.log(posiciones[0]['latitud']);
    console.log('Hay ' + numPosiciones + ' posiciones');
    if(numPosiciones > 0) {
      var hayInternet = System.checkInternet();
      for(contador = 0; contador < numPosiciones; contador++){
        $scope.positions[contador] = posiciones[contador];
        if (hayInternet) {
          var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + colores[contador % 6],
                          new google.maps.Size(21, 34),
                          new google.maps.Point(0,0),
                          new google.maps.Point(10, 34));
          var numMarkers = markers.length;
          markers[numMarkers] = new google.maps.Marker({
            position: new google.maps.LatLng(posiciones[contador]['latitud'], posiciones[contador]['longitud']),
            map: mapa,
            icon: pinImage,
            animation: google.maps.Animation.DROP,
            label: posiciones[contador]['descripcion'],
            title: posiciones[contador]['descripcion']
          });
        }
      }
    } else {
      console.log("No results found");
    }

  }


  $scope.reloadData = function(){

    //Eliminar markers

    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }

    Positions.getPositions().then(function(res){
      pintarPosiciones(res);
    });
  }

  $scope.remove = function(id){
    //Eliminar posicion
    console.log('Vamos a eliminar el registro ' + id);
    var query = "update posiciones set activo = 0 where id = " + id;
    console.log(query);
    $scope.nombre= query;
    $cordovaSQLite.execute(db, query).then(function(res) {
        console.log("Eliminado");
        var posicion = 0;
        console.log('Longitud ' + $scope.positions.length);
        for(posicion = 0; posicion < $scope.positions.length; posicion++){
          console.log('Comparo ' + $scope.positions[posicion].id  + ' con ' + id);
          if($scope.positions[posicion].id == id){
            $scope.positions.splice(posicion, 1);
          }
        }
        positions.splice(positions.indexOf(id), 1);
    }, function (err) {
        console.error(err);
    });
  }
});
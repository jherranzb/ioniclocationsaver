angular.module('starter.services', [])

.factory('Positions', function($cordovaSQLite, $q){
  /*var posiciones = [];
  var query = "SELECT * FROM posiciones where activo = 1";
  $cordovaSQLite.execute(db, query).then(function(res) {
    var contador = 0;
    if(res.rows.length > 0) {
      for(contador = 0; contador < res.rows.length; contador++){
        posiciones[contador] = res.rows.item(contador);
      }
    } else {
      console.log("No results found");
    }
  }, function (err) {
      console.error(err);
  });*/

  return {
    /*getRemotePositions: function($http) {
      console.log('Pidiendo las posiciones remotas....!!!!UFFFFF');
      $http({
        method: 'GET',
        //headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        url: "http://jherranz.sytes.net/locationSaver/getPositions.php",
        data: { 'idUsuario' : idUsuario} 
      })
      .success(function (data, status, headers, config) { 
        console.log('Correcto');
        console.log(data);
      })
      .error(function (data, status, headers, config) { 
        console.log('Error');
        console.log('Error status: ' + status);
        console.log('Error data: ' + data);
      });
    },*/
    getPositions: function() {
      console.log('Demo de Positions funcion getPositions');

      var d = $q.defer();

      var posiciones = [];
      var query = "SELECT * FROM posiciones where activo = 1";
      $cordovaSQLite.execute(db, query).then(function(res) {
        var contador = 0;
        if(res.rows.length > 0) {
          for(contador = 0; contador < res.rows.length; contador++){
            posiciones[contador] = res.rows.item(contador);
          }
          console.log('te saco esto primo');
          d.resolve(posiciones);
        } else {
          console.log("No results found");
        }
      }, function (err) {
          console.error(err);
      });

      return d.promise;
    },
    getDataPositions: function(id) {

      var d = $q.defer();

      var posiciones = [];
      var query = "SELECT * FROM posiciones where id = " + id;
      $cordovaSQLite.execute(db, query).then(function(res) {
        var contador = 0;
        if(res.rows.length > 0) {
          for(contador = 0; contador < res.rows.length; contador++){
            posiciones[contador] = res.rows.item(contador);
          }
          console.log('te saco esto primo');
          d.resolve(posiciones);
        } else {
          console.log("No results found");
        }
      }, function (err) {
          console.error(err);
      });

      return d.promise;
    },
    updatePosition: function(id, descripcion){
      var d = $q.defer();

      var posiciones = [];
      var query = "UPDATE posiciones set descripcion = '" + descripcion + "' where id = " + id;
      console.log(query);
      $cordovaSQLite.execute(db, query).then(function(res) {
        console.log('Posición actualizada correctamente');
        d.resolve();
      }, function (err) {
        console.error(err);
      });

      return d.promise;
    }
  }
})

.factory('System', function(){
  return {
    checkInternet: function(){
      console.log('Entro a ver si hay internes');
      if(window.Connection) {
        if(navigator.connection.type == Connection.NONE) {
          return false;
        } else {
          return true;
        }
      }
    }
  }
})

.factory('Images', function($cordovaSQLite){
  var imagenes = [];
  var query = "SELECT * FROM imagenes";
  $cordovaSQLite.execute(db, query).then(function(res) {
    var contador = 0;
    if(res.rows.length > 0) {
      for(contador = 0; contador < res.rows.length; contador++){
        imagenes[contador] = res.rows.item(contador);
      }
    } else {
      console.log("No results found");
    }
  }, function (err) {
      console.error(err);
  });

  return {
    getImages: function() {
      console.log('Demo de Positions funcion getImages');
      return imagenes;
    }
    /*,
    getImagesFromPosition: function(idPosicion){
      var imagenes = [];
      var numImagenes = imagenes.length;
      var contador = 0;
      for(contador=0; contador < numImagenes; contador++){
        if(imagenes[contador]->idPosicion == idPosicion){
          var numImg = imagenes.length;
          imagenes[numImg] = imagenes[contador];
        }
      }
      return imagenes;
    }*/
  }
})


/*.factory('remoteStorage', function($http){
  return {
    demo: function() {
      console.log('Demo de remoteStorage');
    }
  }
})*/


.factory('Usuarios', function($cordovaSQLite){

  //getUserStore: function() {

    var db = $cordovaSQLite.openDB("my.db");
    var usuarios = [];
    var query = "SELECT * FROM usuarios";
    $cordovaSQLite.execute(db, query).then(function(res) {
      var contador = 0;
      var numRegistros = res.rows.length;
      if(numRegistros > 0) {
        for(contador = 0; contador < numRegistros; contador++){
          usuarios[contador] = res.rows.item(contador);
          console.log('Usuario encontrado!!!!');
          console.log(usuarios[contador]);
        }
      } else {
        console.log("No results found");
      }
    }, function (err) {
        console.error(err);
    });

    return {
      getActiveUserData: function(campo) {
        var contador = 0;
        var numRegistros = usuarios.length;
        for(contador = 0; contador < numRegistros; contador++){
          if(usuarios[contador].activo == 1){
            switch(campo) {
                case 'id':
                    return usuarios[contador].id;
                    break;
                case 'nombre':
                    return usuarios[contador].nombre;
                    break;
                default:
                    return usuarios[contador];
            }
          }
        }
      },

      getAllUsersData: function() {
        return usuarios;
      },

      setActiveUser: function(id) {
        var queryUnset = "UPDATE USUARIOS SET ACTIVO = 0";
        var querySet = "UPDATE USUARIOS SET ACTIVO = 1 WHERE ID = " + id;
        $cordovaSQLite.execute(db, queryUnset);
        $cordovaSQLite.execute(db, querySet);
      }
    }    
  //}
})
.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Camera', ['$q', function($q) {

  return {
    getPicture: function(options) {
      var q = $q.defer();

      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);

      return q.promise;
    }
  }
}]);
